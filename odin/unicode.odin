package main
/*
Unicode map editor

2022-11-27	Started.
*/

import "core:fmt"
import "core:os"

a :: proc(val:bool) -> (bool) {
	fmt.print("\u2794➔");
	return val;
}

getchar :: proc() -> (u8, bool) {
        buf: [6]u8
        n, err  := os.read(os.stdin, buf[:])
	fmt.print(buf[:])
        return buf[0], (err != os.ERROR_NONE || n==0)
}

main :: proc() -> () {
	res : int = 1;
	if( a(true) || a(false) ) {
		res = 0;
	} 
	getchar()
	//return res;
}
