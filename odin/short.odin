package main
/*
Test of shortcircuiting. Odin supports it

2021-12-12	Started. Working
*/

import "core:fmt"

a :: proc(val:bool) -> (bool) {
	fmt.print("!");
	return val;
}

main :: proc() -> () {
	res : int = 1;
	if( a(true) || a(false) ) {
		res = 0;
	} 
	//return res;
}
