package main

import "core:io"
import "core:os"
import "core:fmt"

//Foo :: union {int, bool, u8 }


ch : u8;

State :: enum { Foo, }

getchar :: proc() -> (u8, bool) {
	buf: [1]u8
	n, err  := os.read(os.stdin, buf[:])
	return buf[0], (err != os.ERROR_NONE || n==0)
}

putchar :: proc(c : u8) {
	buf := [1]u8{c}
	os.write(os.stdout, buf[:])
}
	
main :: proc() {
	fmt.println("What say you?");
	for {
		c, err := getchar()
		if(err) { break }
		if ( c == '#' ) {putchar('!')}
		fmt.printf("%c", c);
	}
	fmt.println("Bye for now");

	s : any = 12
	s = "foo"
}
