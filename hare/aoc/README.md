# AoC: Advent of Code 2022

To run, e.g.
```
hare run aoc01.ha < aoc01.txt
```


## References:

* https://adventofcode.com/2022


## Status

 2022-12-01	Started
